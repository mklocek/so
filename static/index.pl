#!/usr/bin/perl
$|++;                              # auto flush output

use CGI;                           # module that simplifies grabbing parameters from query string
use HTML::Entities;                # for character encoding
use strict;                        # to make sure your Perl code is well formed

print qq{Content-type: text/html\n\n};  # http header b/c outputting to browser

   main();                         # calling the function

   sub main{
      my $cgi    = new CGI;        # store all the cgi variables
      # the following stores all the query string variables into a hash
      #   this is unique because you might have multiple query string values for
      #   for a variable. 
      #   (eg http://localhost/?email=a@b.com&email=b@c.com&email=c@d.com )
      my %params = map { $_ => HTML::Entities::encode(join("; ", split("\0", $cgi->Vars->{$_}))) } $cgi->param;

      #Input: http://localhost:9009/?comd&user=kkc&mail=kkc@kkc.com
      #Output: Hello kcc please confirm your email kkc@kkc.com

      print <<HTML;
      <html>
         <head>
            <style type="text/css">.bold{font-weight:bold}</style>
         </head>
         <body>
            Hello <span class="bold">$params{user}</span> please confirm your email <span class="bold">$params{mail}</span>
         </body>
      </html>
HTML

      return 1;
   }
