all: panda

panda: src/panda.c
	gcc -o bin/panda src/panda.c src/logger.c src/network.c src/threads.c

clean:
	$(RM) bin/panda
