### Requirements

- GCC compiler.
- Make.
- Native [epoll](http://man7.org/linux/man-pages/man7/epoll.7.html) support.

### Installation

```
make
```

### Run

```
bin/panda -p PORT -w NUM_WORKERS -r ROOT_DIR
```

### Development

#### Vagrant

##### Installation

[https://www.vagrantup.com/docs/installation/](https://www.vagrantup.com/docs/installation/)

##### Usage

```
vagrant up
```

### Benchmark

PANDA
```
ab -n 10000 -c 768 -k http://127.0.0.1:5001/panda.jpg > benchmarks/panda.ab.concurrency.txt
```

nginx
```
ab -n 10000 -c 768 -k http://127.0.0.1:5002/panda.jpg > benchmarks/nginx.ab.concurrency.txt
```

Apache2
```
ab -n 10000 -c 768 -k http://127.0.0.1:5003/panda.jpg > benchmarks/apache2.concurrency.ab.txt
```