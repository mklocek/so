#include <unistd.h>

#include "threads.h"

void run_child_processes(int num_processes) {
    int p;

    for (p = 0; p < num_processes - 1; p++) {
        if (fork() == 0) {
            break;
        }
    }
}
