#ifndef NETWORK_H_
#define NETWORK_H_

int make_socket_non_blocking(int);
int create_and_bind(char *);

#endif // NETWORK_H_
