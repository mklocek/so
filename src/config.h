#ifndef CONFIG_H_
#define CONFIG_H_

#define ACCESS 1
#define DEBUG 2
#define NOT_FOUND 404
#define FORBIDDEN 433
#define ERROR 500

#define NUM_PROCESSES 1

#define PORT "5001"

#define ROOT_DIR "static/"

#define ACCESS_LOG "log/access.log"
#define ERROR_LOG "log/error.log"
#define DEBUG_LOG "log/debug.log"

#define MAXEVENTS 1000

#define BUF_SIZE 2048

struct config {
    char *port;
    char *root_dir;
    int num_processes;
};

#endif // CONFIG_H_
