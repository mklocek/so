#ifndef REQUEST_H_
#define REQUEST_H_

struct request_h {
    char *header;
    char *method;
    char *file_path;
    char *data;
    char *content_type;
    int content_length;
};

#endif // REQUEST_H_
