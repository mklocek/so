#include <netdb.h>
#include <stdio.h>
#include <memory.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <errno.h>
#include <wait.h>

#include "config.h"
#include "network.h"
#include "extension.h"
#include "logger.h"
#include "request.h"
#include "threads.h"

struct config config = {};

struct extension extensions[] = {
        {"gif",   "image/gif"},
        {"jpg",   "image/jpg"},
        {"jpeg",  "image/jpeg"},
        {"png",   "image/png"},
        {"ico",   "image/ico"},
        {"zip",   "image/zip"},
        {"gz",    "image/gz"},
        {"tar",   "image/tar"},
        {"css",   "text/css"},
        {"map",   "application/json"},
        {"htm",   "text/html"},
        {"html",  "text/html"},
        {"js",    "text/javascript"},
        {"svg",   "image/svg+xml"},
        {"woff",  "font/woff"},
        {"woff2", "font/woff2"},
        {"ttf",   "font/ttf"},
        {0,       0}};

struct request_h parse_request(char *);

void execute_cgi(int, struct request_h);

int serve_get(int, struct request_h);

int serve_post(int, struct request_h);

void resp_error(int type, int socket_fd) {
    char resp_buf[BUF_SIZE];
    char file_buf[BUF_SIZE];
    long len;
    FILE *file;
    size_t nread;

    memset(resp_buf, 0, sizeof resp_buf);
    memset(file_buf, 0, sizeof file_buf);

    char *file_path, *response;

    switch (type) {
        case ERROR:
            file_path = "500.html";
            response = "500 Server Error";
            break;
        case FORBIDDEN:
            file_path = "403.html";
            response = "403 Forbidden";
            break;
        case NOT_FOUND:
            file_path = "404.html";
            response = "404 Not Found";
            break;
    }

    char *path = (char *) malloc(1 + strlen(config.root_dir) + strlen(file_path));
    strcpy(path, config.root_dir);
    strcat(path, &file_path[1]);

    file = fopen(path, "r");

    if (file) {
        fseek(file, 0L, SEEK_END);
        len = ftell(file);
        rewind(file);

        sprintf(resp_buf, ""
                        "HTTP/1.1 %s\n"
                        "Server: PANDA\n"
                        "Content-Length: %ld\n"
                        "Connection: keep-alive\n"
                        "Content-Type: text/html\n\n",
                response,
                len);

        if (send(socket_fd, resp_buf, strlen(resp_buf), MSG_NOSIGNAL)) {
            while ((nread = fread(file_buf, 1, sizeof file_buf, file)) > 0) {
                if (!send(socket_fd, file_buf, nread, MSG_NOSIGNAL)) {
                    break;
                }
            }
        }

        fclose(file);
    } else {
        switch (type) {
            case ERROR:
                sprintf(resp_buf, ""
                                "HTTP/1.1 %s\n"
                                "Server: PANDA\n"
                                "Content-Length: 141\n"
                                "Connection: keep-alive\n"
                                "Content-Type: text/html\n\n"
                                "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><title>PANDA - 500 Server Error</title></head><body>500 Server Error</body></html>",
                        response);
                break;
            case FORBIDDEN:
                sprintf(resp_buf, ""
                                "HTTP/1.1 %s\n"
                                "Server: PANDA\n"
                                "Content-Length: 135\n"
                                "Connection: keep-alive\n"
                                "Content-Type: text/html\n\n"
                                "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><title>PANDA - 403 Forbidden</title></head><body>403 Forbidden</body></html>",
                        response);
                break;
            case NOT_FOUND:
                sprintf(resp_buf, ""
                                "HTTP/1.1 %s\n"
                                "Server: PANDA\n"
                                "Content-Length: 135\n"
                                "Connection: keep-alive\n"
                                "Content-Type: text/html\n\n"
                                "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><title>PANDA - 404 Not Found</title></head><body>404 Not Found</body></html>",
                        response);
                break;
        }

        send(socket_fd, resp_buf, strlen(resp_buf), MSG_NOSIGNAL);
    }
}

int resp_success(int fd, char *file_path, char *file_type) {
    char resp_buf[BUF_SIZE];
    char file_buf[BUF_SIZE];
    char *file_name;
    long len;
    FILE *file;
    size_t nread;

    memset(resp_buf, 0, sizeof resp_buf);
    memset(file_buf, 0, sizeof file_buf);

    char *path = (char *) malloc(1 + strlen(config.root_dir) + strlen(file_path));
    strcpy(path, config.root_dir);
    strcat(path, &file_path[1]);

    file = fopen(path, "r");

    if (file) {
        fseek(file, 0L, SEEK_END);
        len = ftell(file);
        rewind(file);

        sprintf(resp_buf, ""
                        "HTTP/1.1 200 OK\n"
                        "Server: PANDA\n"
                        "Content-Type: %s\n"
                        "Content-Length: %ld\n"
                        "Connection: keep-alive\n\n",
                file_type,
                len);

        if (send(fd, resp_buf, strlen(resp_buf), MSG_NOSIGNAL)) {
            while ((nread = fread(file_buf, 1, sizeof file_buf, file)) > 0) {
                if (!send(fd, file_buf, nread, MSG_NOSIGNAL)) {
                    fclose(file);
                    return 0;
                }
            }
        } else {
            fclose(file);
            return 0;
        }

        fclose(file);
    } else {
        resp_error(NOT_FOUND, fd);
        logger(ERROR_LOG, NOT_FOUND, "failed to open file", file_path);
    }

    return 1;
}

/**
 *
 * @param buf
 * @param orginal_buffer
 * @return last char position or 0 if end of file
 */
int get_line(char *buf, char *orginal_buffer) {
    int i = 0;
    char c = '\0';

    while ((c != '\n')) {
        if (strlen(&orginal_buffer[i]) > 0) {
            c = orginal_buffer[i];
            if (c == '\r') {
                if (strlen(&orginal_buffer[i + 1]) > 0) {
                    c = orginal_buffer[i + 1];
                    if (c == '\n') {
                        i++;
                    } else {
                        c = '\n';
                    }
                } else {
                    c = '\n';
                }
            }
            buf[i] = c;
            i++;
        } else {
            c = '\n';
        }
    }
    buf[i] = '\0';

    if (strlen(&orginal_buffer[i]) == 0) {
        i = 0;
    }
    return (i);
}

void event_loop(int sfd) {
    int s, efd;
    struct epoll_event event, *events;

    efd = epoll_create1(0);

    if (efd == -1) {
        abort();
    }

    event.data.fd = sfd;
    event.events = EPOLLIN | EPOLLET;

    s = epoll_ctl(efd, EPOLL_CTL_ADD, sfd, &event);

    if (s == -1) {
        printf("epoll_ctl");
        abort();
    }

    // Buffer where events are returned
    events = calloc(MAXEVENTS, sizeof event);

    // The event loop
    while (1) {
        int n, i;

        n = epoll_wait(efd, events, MAXEVENTS, -1);

        for (i = 0; i < n; i++) {
            int fd = events[i].data.fd;

            if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP) || (!(events[i].events & EPOLLIN))) {
                // An error has occured on this fd, or the socket is not ready for reading (why were we notified then?)
                close(fd);
                continue;
            } else if (sfd == fd) {
                // We have a notification on the listening socket, which means one or more incoming connections.
                while (1) {
                    struct sockaddr in_addr;
                    socklen_t in_len;
                    int infd;
                    char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];

                    in_len = sizeof in_addr;
                    infd = accept(sfd, &in_addr, &in_len);

                    if (infd == -1) {
                        if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
                            // We have processed all incoming connections.
                            break;
                        } else {
                            break;
                        }
                    }

                    s = getnameinfo(&in_addr, in_len, hbuf, sizeof hbuf, sbuf, sizeof sbuf,
                                    NI_NUMERICHOST | NI_NUMERICSERV);

                    // Make the incoming socket non-blocking and add it to the list of fds to monitor.
                    s = make_socket_non_blocking(infd);

                    if (s == -1) {
                        abort();
                    }

                    event.data.fd = infd;
                    event.events = EPOLLIN | EPOLLET;

                    s = epoll_ctl(efd, EPOLL_CTL_ADD, infd, &event);

                    if (s == -1) {
                        printf("epoll_ctl");
                        abort();
                    }
                }

//                continue;
            } else {
                /* We have data on the fd waiting to be read. Read and
                   display it. We must read whatever data is available
                   completely, as we are running in edge-triggered mode
                   and won't get a notification again for the same
                   data. */

                int should_close = 0;
                struct request_h request;

                ssize_t count;
                char req_buf[BUF_SIZE];

                memset(req_buf, 0, sizeof req_buf);

                count = read(fd, req_buf, sizeof req_buf);

                if (count == -1) {
                    // If errno == EAGAIN, that means we have read all data. So go back to the main loop.
                    if (errno != EAGAIN && errno != EWOULDBLOCK) {
                        should_close = 1;
                    }
                    break;
                } else if (count == 0) {
                    should_close = 1;
                    break;
                } else {

                    request = parse_request(req_buf);

                    if ((strcasecmp(request.method, "GET") == 0)) {
                        if (serve_get(fd, request) == 0) {
                            should_close = 1;
                        }
                    } else if ((strcasecmp(request.method, "POST") == 0)) {
                        if (serve_post(fd, request) == 0) {
                            should_close = 1;
                        }
                    } else {
                        // we support only GET and POST
                        logger(ERROR_LOG, FORBIDDEN, "Method not operation supported", req_buf);
                        should_close = 1;
                    }
                }

                if (should_close) {
                    // closing means we probably had some error
                    // normally, we keep connection open for another requests
                    close(fd);
                }
            }
        }
    }

    free(events);
}

int serve_post(int fd, struct request_h request) {
    char resp_buf[BUF_SIZE];
    int cont_length = request.content_length;
    if (cont_length && (request.content_length != strlen(request.data))) {
        resp_error(FORBIDDEN, fd);
        logger(ERROR_LOG, FORBIDDEN, "content-length mismatch", "");
        return 0;
    }
    sprintf(resp_buf, ""
                    "HTTP/1.1 200 OK\r\n"
                    "Server: PANDA\r\n"
                    "Content-Type: %s\r\n"
                    "Content-Length: %d\r\n"
                    "Connection: keep-alive\r\n\r\n%s",
            request.content_type,
            request.content_length,
            request.data);

    if (!send(fd, resp_buf, strlen(resp_buf), MSG_NOSIGNAL)) {
        return 0;
    }

    return 1;
}

int serve_get(int fd, struct request_h request) {
    int j, buflen;
    long len;
    char *file_type;
    int path_length = (int) strlen(request.file_path);

    for (j = 0; j < path_length - 1; j++) {
        // check for illegal parent directory use ..
        if (request.file_path[j] == '.' && request.file_path[j + 1] == '.') {
            resp_error(FORBIDDEN, fd);

            logger(ERROR_LOG, FORBIDDEN, "Parent directory (..) path names not supported", request.file_path);

            return 0;
        }
    }
    if (!strcmp(&request.file_path[path_length - 4],".cgi") && request.data != NULL) {
        execute_cgi(fd, request);
        return 1;
    }
    // work out the file type and check we support it
    buflen = (int) strlen(request.file_path);
    file_type = (char *) 0;

    for (j = 0; extensions[j].ext != 0; j++) {
        len = strlen(extensions[j].ext);

        if (!strncmp(&request.file_path[buflen - len], extensions[j].ext, len)) {
            file_type = extensions[j].filetype;
            break;
        }
    }

    if (file_type == 0) {
        resp_error(FORBIDDEN, fd);
        logger(ERROR_LOG, FORBIDDEN, "file extension type not supported", request.file_path);

        return 0;
    }

    return resp_success(fd, request.file_path, file_type);
}

struct request_h parse_request(char *req_buf) {
    struct request_h request;
    int line_position;
    size_t file_start_position, file_end_position, memory_lock;
    char *buffer_pointer = req_buf, header[BUF_SIZE] = "";

    request.method = NULL;
    request.file_path = NULL;
    request.header = NULL;
    request.data = NULL;
    request.content_type = NULL;

    line_position = get_line(header, req_buf);

    if (line_position == 0) { return request; }

    request.header = (char *) malloc(strlen(header) + 1);
    stpcpy(request.header, header);

    file_start_position = (size_t) (strstr(header, " ") - header);

    // Parse method (GET, POST ...)
    request.method = (char *) malloc(file_start_position + 1);
    memset(request.method, 0, sizeof(request.method));
    strncpy(request.method, header, file_start_position);

    // Parse file path
    if (!strncmp(&header[file_start_position], " / ", 3)) {
        request.file_path = (char *) malloc(12);
        strcpy(request.file_path, "/index.html");
    } else {
        char *end_char = strstr(&header[file_start_position + 1], " ");
        char *params_char = strstr(&header[file_start_position + 1], "?");
        if (end_char != NULL) {
            file_end_position = (size_t) (end_char - header);

            if (params_char != NULL) {
                size_t params_end_position = file_end_position;
                file_end_position = (size_t) (params_char - header);
                memory_lock = params_end_position - file_end_position;
                request.data = (char *) malloc(memory_lock);
                memset(request.data, 0, memory_lock);
                strncpy(request.data, &header[file_end_position + 1], memory_lock - 1);
            }
            memory_lock = file_end_position - file_start_position;
            request.file_path = (char *) malloc(memory_lock);
            memset(request.file_path, 0, memory_lock);
            strncpy(request.file_path, &header[file_start_position + 1], memory_lock - 1);
        }
    }
    while (line_position > 0) {
        memset(header, 0, sizeof(header));
        line_position = get_line(header, buffer_pointer);
        buffer_pointer += line_position;

        if (!strncasecmp(header, "Content-Length:", 15)) {
            request.content_length = atoi(&(header[16]));
        }

        if (!strncasecmp(header, "Content-Type:", 13)) {
            memory_lock = strlen(header) - 13;
            request.content_type = malloc(memory_lock);
            memset(request.content_type, 0, memory_lock);
            strcpy(request.content_type, &header[14]);
        }

    }

    if (!strncasecmp(request.method, "POST", 4) && request.content_length) {
        memory_lock = (size_t) (request.content_length) + 1;
        request.data = (char *) malloc(memory_lock);
        strcpy(request.data, header);
    }
    return request;
}

void execute_cgi(int fd, struct request_h request) {
    char buf[1024];
    int cgi_output[2];
    int cgi_input[2];
    pid_t pid;
    int status;
    char c;

    sprintf(buf, ""
            "HTTP/1.1 200 OK\r\n");
    send(fd, buf, strlen(buf), 0);

    if (pipe(cgi_output) < 0) {
        //  cannot_execute;
        return;
    }
    if (pipe(cgi_input) < 0) {
        // cannot_execute;
        return;
    }

    if ((pid = fork()) < 0) {
        //cannot_execute;
        return;
    }
    if (pid == 0)  /* child: CGI script */
    {
        char meth_env[255];
        char query_env[255];
        char length_env[255];

        dup2(cgi_output[1], 1);
        dup2(cgi_input[0], 0);
        close(cgi_output[0]);
        close(cgi_input[1]);
        sprintf(meth_env, "REQUEST_METHOD=%s", request.method);
        putenv(meth_env);
        if (strcasecmp(request.method, "GET") == 0) {
            sprintf(query_env, "QUERY_STRING=%s", request.data);
            putenv(query_env);
        } else {   /* POST */
            sprintf(length_env, "CONTENT_LENGTH=%d", request.content_length);
            putenv(length_env);
        }

        char *path = (char *) malloc(1 + strlen(config.root_dir) + strlen(request.file_path));
        strcpy(path, config.root_dir);
        strcat(path, &request.file_path[1]);

        execl(path, path, NULL);
        exit(0);
    } else {    /* parent */
        close(cgi_output[1]);
        close(cgi_input[0]);

        if (strcasecmp(request.method, "POST") == 0) {
            write(cgi_input[1], request.data, (size_t) request.content_length);
        }

        while (read(cgi_output[0], &c, 1) > 0) {
            send(fd, &c, 1, MSG_NOSIGNAL);
        }

        close(cgi_output[0]);
        close(cgi_input[1]);
        waitpid(pid, &status, 0);
        close(fd);
    }
}

void get_config(int argc, char *argv[]) {
    int opt, num_processes;

    while ((opt = getopt(argc, argv, "p:r:w:")) != -1) {
        switch (opt) {
            case 'p':
                config.port = optarg;
                break;
            case 'r':
                if (optarg[strlen(optarg) - 1] != '/') {
                    char *slash = "/";

                    config.root_dir = (char *) malloc(1 + strlen(optarg) + strlen(slash));
                    strcpy(config.root_dir, optarg);
                    strcat(config.root_dir, slash);
                } else {
                    config.root_dir = optarg;
                }
                break;
            case 'w':
                if ((num_processes = atoi(optarg)) > 0) {
                    config.num_processes = num_processes;
                    break;
                } else {
                    printf("Invalid option received\n");
                    exit(EXIT_FAILURE);
                }
            case '?':
                printf("Invalid option received\n");
                exit(EXIT_FAILURE);
        }
    }

    if (!config.port) {
        config.port = PORT;
    }

    if (!config.root_dir) {
        config.root_dir = ROOT_DIR;
    }

    if (!config.num_processes) {
        config.num_processes = NUM_PROCESSES;
    }
}

int main(int argc, char *argv[]) {
    int sfd, s;

    get_config(argc, argv);

    sfd = create_and_bind(config.port);
    if (sfd == -1) abort();

    s = make_socket_non_blocking(sfd);
    if (s == -1) abort();

    s = listen(sfd, SOMAXCONN);
    if (s == -1) abort();

    printf("It's PANDA Server!\n\n"
                   "Listening on port: %s\n"
                   "Serving content from: %s\n"
                   "Number of workers: %d\n\n"
                   "Press Ctrl+C to stop the server\n",
           config.port,
           config.root_dir,
           config.num_processes
    );

    run_child_processes(config.num_processes);

    event_loop(sfd);

    close(sfd);

    return EXIT_SUCCESS;
}
