#include <sys/fcntl.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "logger.h"
#include "config.h"

void logger(char *file_name, int type, char *s1, char *s2) {
    int fd;
    char log_buf[512 + strlen(s1) + strlen(s2)];

    switch (type) {
        case ERROR:
            sprintf(log_buf, "ERROR: %s:%s Errno=%d exiting pid=%d\n\n", s1, s2, errno, getpid());
            break;
        case FORBIDDEN:
            sprintf(log_buf, "FORBIDDEN: %s:%s\n\n", s1, s2);
            break;
        case NOT_FOUND:
            sprintf(log_buf, "NOT FOUND: %s:%s\n\n", s1, s2);
            break;
        case ACCESS:
            sprintf(log_buf, "INFO: %s:%s\n\n", s1, s2);
            break;
        case DEBUG:
            sprintf(log_buf, "DEBUG: %s:%s\n\n", s1, s2);
            break;
    }

    if ((fd = open(file_name, O_CREAT | O_WRONLY | O_APPEND, 0644)) >= 0) {
        write(fd, log_buf, strlen(log_buf));
        close(fd);
    }
}